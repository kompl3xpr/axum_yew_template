use yew::{prelude::*, Renderer};
use yew_router::prelude::*;

mod not_found;
use not_found::*;

mod home;
use home::*;

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,

    #[not_found]
    #[at("/not_found")]
    NotFound,
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <Home/> },
        Route::NotFound => html! { <NotFound/> },
    }
}

fn main() {
    #[function_component(App)]
    fn app() -> Html {
        html! { <BrowserRouter><Switch<Route> render={switch} /></BrowserRouter> }
    }
    Renderer::<App>::new().render();
}