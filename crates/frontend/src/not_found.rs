use yew::prelude::*;
use gloo::utils::document;

#[function_component(NotFound)]
pub fn not_found() -> Html {
    document().set_title(&"404 Not Found - My Web App");
    html! {
        <h1>{"Error 404: Not Found"}</h1>
    }
}