use gloo::{
    net::http::Request,
    utils::document,
};
use mywebapp_core::{Tagged, User, UserLoginRecord};
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::spawn_local;
use web_sys::HtmlInputElement;
use yew::prelude::*;

async fn fetch_username(username: UseStateHandle<Option<String>>) {
    let response = Request::get("/api/v1/whoami").send().await;

    if let Ok(resp) = response {
        let user = resp.json::<Tagged<Option<User>>>().await.unwrap();
        username.set(user.data.map(|u| u.username));
    }
}

async fn post_logout() {
    Request::post("/api/v1/logout").send().await.ok();
}

#[function_component(Home)]
pub fn home() -> Html {
    document().set_title(&"My Web App");

    let username = use_state(|| None::<String>);
    let updater = use_state(|| false);
    {
        let updater = updater.clone();
        let username = username.clone();
        use_effect_with_deps(move |_| spawn_local(fetch_username(username)), updater);
    }

    let logout = {
        let updater = updater.clone();
        move |_| {
            spawn_local(post_logout());
            updater.set(!*updater);
        }
    };

    let login = move |_| {
        let elem = document().get_element_by_id("username").unwrap();
        let username = elem.dyn_into::<HtmlInputElement>().unwrap().value();
        let elem = document().get_element_by_id("password").unwrap();
        let password = elem.dyn_into::<HtmlInputElement>().unwrap().value();

        let request = Request::post("/api/v1/login").json(&UserLoginRecord { username, password });
        spawn_local(async move {
            request.unwrap().send().await.ok();
        });
        updater.set(!*updater);
    };

    html! {
        if let Some(username) = (*username).as_ref() {
            <h1>{"Hello, "}{username}{"."}</h1>
            <button onclick={logout}>{"Log Out"}</button>
        } else {
            <h1>{"Welcome!"}</h1>
            <label for="username">{"username: "}</label>
            <input type="text" id="username" name="username"/><br/>
            <label for="password">{"password: "}</label>
            <input type="password" id="password" name="password"/><br/>
            <button onclick={login}>{"Log In"}</button>
        }
    }
}
