use serde::{Serialize, Deserialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Error {
    InternalDatabaseError,

    UsernameNotFound,
    UsernameAlreadyRegistered,
    WrongPassword,
}