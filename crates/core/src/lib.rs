use serde::{Serialize, Deserialize};
mod tag;
pub use tag::*;
mod error;
pub use error::*;


impl_tag!(User: "data"."user");
#[derive(sqlx::FromRow, Debug, Clone, Serialize, Deserialize)]
pub struct User {
    pub user_id: i32,
    pub username: String,
    pub password_hash: String,
}

impl_tag!(UserLoginRecord: "data"."user_login_record");
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserLoginRecord {
    pub username: String,
    pub password: String,
}
