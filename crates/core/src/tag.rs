use serde::{Deserialize, Serialize};

pub trait HasTag {
    fn tag() -> &'static str;
    fn id() -> &'static str;
    fn namespace() -> &'static str;
    fn optional_tag() -> &'static str;
    fn optional_id() -> &'static str;
    fn result_tag() -> &'static str;
    fn result_id() -> &'static str;
}

macro_rules! impl_tag {
    ($t: ty: $namespace: literal. $id: literal) => {
        impl HasTag for $t {
            fn tag() -> &'static str { concat!($namespace, ".", $id) }
            fn id() -> &'static str { $id }
            fn namespace() -> &'static str { $namespace }
            fn optional_tag() -> &'static str { concat!($namespace, ".optional.", $id) }
            fn optional_id() -> &'static str { concat!("optional.", $id) }
            fn result_tag() -> &'static str { concat!($namespace, ".result.", $id) }
            fn result_id() -> &'static str { concat!("result.", $id) }
        }
    };
}

pub(crate) use impl_tag;

use crate::Error;
impl_tag!(bool: "data"."bool");
impl_tag!(String: "data"."string");
impl_tag!(&'static str: "data"."string");
impl_tag!(i8: "data"."i8");
impl_tag!(i16: "data"."i16");
impl_tag!(i32: "data"."i32");
impl_tag!(i64: "data"."i64");
impl_tag!(u8: "data"."u8");
impl_tag!(u16: "data"."u16");
impl_tag!(u32: "data"."u32");
impl_tag!(u64: "data"."u64");
impl_tag!((): "data"."unit");

impl<T: HasTag> HasTag for Option<T> {
    fn tag() -> &'static str {
        T::optional_tag()
    }
    fn id() -> &'static str {
        T::id()
    }
    fn namespace() -> &'static str {
        T::namespace()
    }
    fn optional_tag() -> &'static str {
        panic!("Unsupported nested optional value")
    }

    fn optional_id() -> &'static str {
        panic!("Unsupported nested optional value")
    }
    fn result_tag() -> &'static str {
        panic!("Unsupported nested result value")
    }
    fn result_id() -> &'static str {
        panic!("Unsupported nested result value")
    }
}

impl<T: HasTag> HasTag for Result<T, Error> {
    fn tag() -> &'static str {
        T::result_tag()
    }
    fn id() -> &'static str {
        T::result_id()
    }
    fn namespace() -> &'static str {
        T::namespace()
    }
    fn optional_tag() -> &'static str {
        panic!("Unsupported nested optional value")
    }

    fn optional_id() -> &'static str {
        panic!("Unsupported nested optional value")
    }
    fn result_tag() -> &'static str {
        panic!("Unsupported nested result value")
    }
    fn result_id() -> &'static str {
        panic!("Unsupported nested result value")
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Tagged<T> {
    pub tag: String,
    pub data: T,
}

impl<T> Tagged<T> {
    pub fn with_tag(tag: &'static str, data: T) -> Self {
        Self { tag: tag.to_string(), data }
    }
}

impl<T: HasTag> Tagged<T> {
    pub fn new(data: T) -> Tagged<T> {
        Self {
            tag: T::tag().to_string(),
            data,
        }
    }
}

pub fn tagged<T: HasTag>(data: T) -> Tagged<T> {
    Tagged::new(data)
}
