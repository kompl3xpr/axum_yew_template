use axum::{Router, Extension};
use tracing::info;
use tap::prelude::*;

use crate::config::CONFIG;

pub async fn add_db_layer(router: Router) -> Router {
    info!("connecting database...");
    let db = sqlx::postgres::PgPoolOptions::new()
        .max_connections(5)
        .connect(&CONFIG.database.uri)
        .await
        .expect("Failed to connect the database")
        .pipe(|db| Box::leak(Box::new(db)));
    info!("database connected.");

    info!("running migrations...");
    sqlx::migrate!("../../db/migrations")
        .run(db as &_)
        .await
        .expect("Failed to run migrations");
    info!("migrations done.");

    router.layer(Extension(db as &_))
}