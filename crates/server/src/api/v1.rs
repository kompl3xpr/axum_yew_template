use axum::{extract::Path, http::StatusCode, response::IntoResponse, Extension, Json};
use axum_sessions::{
    async_session::{
        base64,
        sha2::{Digest, Sha256},
    },
    extractors::{ReadableSession, WritableSession},
};
use mywebapp_core::{tagged, Error, User, UserLoginRecord};
use sqlx::{postgres::PgPool, query, query_as};
use tap::prelude::*;

fn hashed(password: &str) -> String {
    Sha256::new()
        .tap_mut(|sha| sha.update(password))
        .finalize()
        .pipe(base64::encode)
}

pub async fn get_user_by_id(
    Extension(db): Extension<&PgPool>,
    Path(user_id): Path<i32>,
) -> impl IntoResponse {
    let result: Result<User, _> = query_as(r#"SELECT * FROM "user" WHERE user_id=$1"#)
        .bind(user_id)
        .fetch_one(db)
        .await;

    let status_code = result
        .as_ref()
        .map_or(StatusCode::NOT_FOUND, |_| StatusCode::OK);
    (status_code, Json(tagged(result.ok())))
}

pub async fn who_am_i(
    session: ReadableSession,
    Extension(db): Extension<&PgPool>,
) -> impl IntoResponse {
    let Some(user_id): Option<i32> = session.get("user_id") else {
        return (StatusCode::NO_CONTENT, Json(tagged(None)));
    };
    let result: Result<User, _> = query_as(r#"SELECT * FROM "user" WHERE user_id=$1"#)
        .bind(user_id)
        .fetch_one(db)
        .await;

    let status_code = match result {
        Ok(_) => StatusCode::OK,
        Err(_) => StatusCode::NOT_FOUND,
    };
    (status_code, Json(tagged(result.ok())))
}

pub async fn register(
    Extension(db): Extension<&PgPool>,
    Json(data): Json<UserLoginRecord>,
) -> impl IntoResponse {
    let UserLoginRecord { username, password } = data;
    let result = query(r#"INSERT INTO "user" (username, password_hash) VALUES ($1, $2)"#)
        .bind(username)
        .bind(hashed(&password))
        .execute(db)
        .await;

    let mut status_code = StatusCode::CREATED;
    result
        .map(|_| ())
        .map_err(|e| match e {
            sqlx::Error::Database(db_err)
                if db_err.code().as_ref().map(AsRef::as_ref) == Some("23505") =>
            {
                status_code = StatusCode::CONFLICT;
                Error::UsernameAlreadyRegistered
            }
            _ => {
                status_code = StatusCode::INTERNAL_SERVER_ERROR;
                Error::InternalDatabaseError
            }
        })
        .pipe(|r| (status_code, Json(tagged(r))))
}

pub async fn login(
    mut session: WritableSession,
    Extension(db): Extension<&PgPool>,
    Json(data): Json<UserLoginRecord>,
) -> impl IntoResponse {
    let UserLoginRecord { username, password } = data;
    let result: Result<User, _> = query_as(r#"SELECT * FROM "user" WHERE username=$1"#)
        .bind(username)
        .fetch_one(db)
        .await;

    let mut status_code = StatusCode::OK;
    let result = result
        .map_err(|_| {
            status_code = StatusCode::NOT_FOUND;
            Error::UsernameNotFound
        })
        .and_then(|user| {
            if user.password_hash == hashed(&password) {
                Ok(user)
            } else {
                status_code = StatusCode::UNAUTHORIZED;
                Err(Error::WrongPassword)
            }
        })
        .map(|user| {
            session.remove("user_id");
            session.insert("user_id", user.user_id).ok();
        });

    (status_code, Json(tagged(result)))
}

pub async fn logout(mut session: WritableSession) -> impl IntoResponse {
    let result = matches!(session.get_raw("user_id"), Some(_));
    let status_code = if result {
        session.remove("user_id");
        StatusCode::OK
    } else {
        StatusCode::NO_CONTENT
    };
    (status_code, Json(tagged(result)))
}
