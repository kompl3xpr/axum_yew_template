mod api;
mod config;
mod db;
mod service;
mod session;

use axum::{
    routing::{get, post},
    Router, Server,
};
use config::*;
use tap::prelude::*;
use tracing::info;

pub fn route_api(router: Router) -> Router {
    router.nest(
        "/api/v1",
        Router::new()
            .route("/whoami", get(api::v1::who_am_i))
            .route("/user/:id", get(api::v1::get_user_by_id))
            .route("/register", post(api::v1::register))
            .route("/login", post(api::v1::login))
            .route("/logout", post(api::v1::logout)),
    )
}

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        .init();

    let app = Router::new()
        .pipe(route_api)
        .pipe(session::add_session_layer)
        .pipe(db::add_db_layer)
        .await
        .layer(tower_http::trace::TraceLayer::new_for_http())
        .fallback_service(get(service::yew_service));

    let socket_addr = ([127, 0, 0, 1], CONFIG.server.port).into();
    info!("running server on `{socket_addr}`...");
    Server::bind(&socket_addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}
