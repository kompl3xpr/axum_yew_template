use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};

pub const CONFIG: Lazy<Config> = Lazy::new(|| {
    toml::de::from_str(
        std::fs::read_to_string("./Server.toml")
            .expect("`Server.toml` not found")
            .as_str(),
    )
    .expect("Failed to parse config from `Server.toml`")
});

#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    pub server: ServerConfig,
    pub database: DatabaseConfig,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ServerConfig {
    pub port: u16,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct DatabaseConfig {
    pub uri: String,
}