use axum::{
    body::{boxed as boxed_body, Body},
    http::{Request, Response, StatusCode},
    response::IntoResponse,
};
use std::path::Path;
use tower::ServiceExt;
use tower_http::services::{ServeDir, ServeFile};

pub const STATIC_DIR: &str = "./dist";

pub async fn yew_service(req: Request<Body>) -> impl IntoResponse {
    let static_dir = Path::new(STATIC_DIR);

    let req2 = Request::builder()
        .method("GET")
        .uri(req.uri())
        .version(req.version())
        .body(())
        .unwrap();

    let err_resp = |err| {
        Response::builder()
            .status(StatusCode::INTERNAL_SERVER_ERROR)
            .body(boxed_body(Body::from(format!("Error: {err}"))))
            .unwrap()
    };

    match ServeDir::new(static_dir).oneshot(req2).await {
        Ok(resp) => {
            let status = resp.status();
            match status {
                StatusCode::NOT_FOUND => {
                    let index = ServeFile::new(static_dir.join("index.html"));
                    index
                        .oneshot(req)
                        .await
                        .map_or_else(err_resp, |r| r.map(boxed_body))
                }
                _ => resp.map(boxed_body),
            }
        }
        Err(err) => err_resp(err),
    }
}
