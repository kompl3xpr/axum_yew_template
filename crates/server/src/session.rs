use axum::Router;
use axum_sessions::{SessionLayer, async_session::{MemoryStore, log::info}};
use rand_chacha::rand_core::{self, SeedableRng, RngCore};
use tap::Tap;

pub type Secret = [u8; 64];

fn generate_secret() -> Secret {
    let mut rng = rand_chacha::ChaChaRng::seed_from_u64(rand_core::OsRng.next_u64());
    [0u8; 64].tap_mut(|b| rng.fill_bytes(b))
}

pub fn add_session_layer(router: Router) -> Router {
    info!("initializing session store...");
    router
        .layer(SessionLayer::new(MemoryStore::new(), &generate_secret()))
        .tap(|_| info!("session store initialized."))
}