# Rust Web Project Template with Axum, Yew, and SQLx


Welcome to the `axum-yew-template`! This template provides a foundation for building modern web applications using a combination of powerful Rust libraries and frameworks, including Axum, Yew, and SQLx with Postgres integration. 

## Features

- **Axum:** A high-performance asynchronous web framework for Rust, designed to handle the demands of modern web applications. Axum enables you to write clean, concise, and efficient asynchronous code while handling routing, middleware, and more.
- **Yew:** A modern front-end framework for Rust that allows you to build dynamic and interactive user interfaces using a component-based architecture. Yew leverages Rust's safety guarantees and provides a productive development experience.
- **SQLx:** SQLx is a database access library for Rust that provides a convenient and safe way to interact with databases using SQL queries. This template includes integration with the Postgres database using SQLx, making it easy to manage and query your application's data.

## Getting Started

Follow these steps to set up your project:

1. **Clone the Repository:** Start by cloning this repository to your local machine using the following command:

   ```
   git clone https://gitlab.com/kompl3xpr/axum_yew_template
   ```

2. **Install Dependencies:** Navigate to the project directory and install the necessary dependencies:

   ```
   cd axum_yew_template
   cargo install --locked trunk
   ```

3. **Configure Database:** Rename `Server.toml.template` to `Server.toml` and update the database configuration to match your Postgres database settings.


4. **Run the Server:**

   ```
   trunk build
   cargo run
   ```

5. **Access Your App:** Open your web browser and go to `http://localhost:8080` to see your Rust web app in action!

## Project Structure

The template follows a structured organization to keep your code organized and maintainable:

- `crates/server/`: Contains the Axum-based backend server code.
- `static/`: Stores static files.
- `crates/frontend/`: Houses the Yew-based frontend code.
- `db/migrations/`: Stores database migration scripts for SQLx.
- `crates/core/`: Includes shared code and data models used by both the backend and frontend.

## Contributions and Feedback

We welcome contributions, bug reports, and feedback from the community! If you encounter any issues or have suggestions for improvements, please feel free to [open an issue](https://gitlab.com/kompl3xpr/axum_yew_template/issues) on the GitHub repository.

## License

This project is licensed under the [MIT License](LICENSE), allowing you to use, modify, and distribute the code as you see fit.

---

Thank you for choosing this template! We hope it accelerates your development process and helps you build amazing web applications with Rust. If you have any questions or need assistance, don't hesitate to reach out.

Happy coding! 🚀